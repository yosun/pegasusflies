﻿using UnityEngine;
using System.Collections;

/// <summary>
/// グローバルオブジェクトの管理クラス
/// </summary>
public class GlobalManager : DontDestroySingletonMonoBehaviour<GlobalManager>
{
    /// <summary>
    /// 生成するPrefabの配列
    /// </summary>
    public GameObject[] Prefabs;

    // 初期化処理
    new void Awake()
    {
        base.Awake();
        
        // プレハブの生成
        if(Instance == this)
        {
            foreach (GameObject obj in Prefabs)
            {
                Instantiate(obj).transform.parent = transform;
            }
        }
    }

    // シーン開始時の処理
    void Start()
    {
        // 全オブジェクトの有効化
        if (Instance == this)
        {
            gameObject.SetActive(true);
        }
    }
}
