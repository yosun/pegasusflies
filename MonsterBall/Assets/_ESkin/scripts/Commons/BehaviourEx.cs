﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 拡張Behaviourクラス
/// </summary>
public abstract class BehaviourEx : MonoBehaviour {

    // オブジェクトを無効化し、指定秒数後破棄する。
    public void DelayDestroy(float delaySencods)
    {
        Invoke("DelayDestroyThisObject", delaySencods);
    }

    /// <summary>
    /// オブジェクトの遅延破棄
    /// </summary>
    protected void DelayDestroyThisObject()
    {
        Destroy(gameObject);
    }
}
