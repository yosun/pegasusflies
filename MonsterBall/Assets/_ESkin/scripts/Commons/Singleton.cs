﻿using UnityEngine;
using System.Collections;

/// <summary>
/// シングルトンパターンを実装するための基本クラス
/// </summary>
/// <typeparam name="T">拡張クラス自身を指定する</typeparam>
public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _Instance;

    public static T Instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = (T)FindObjectOfType(typeof(T));
                if (_Instance == null)
                {
                    Debug.LogError(typeof(T) + "is nothing");
                }
            }
            return _Instance;
        }
    }
}

/// <summary>
/// シーン間で共有されるシングルトンパターンを実装するための基本クラス
/// </summary>
/// <typeparam name="T">拡張クラス自身を指定する</typeparam>
public class DontDestroySingletonMonoBehaviour<T> : SingletonMonoBehaviour<T> where T : SingletonMonoBehaviour<T>
{
    public void Awake()
    {
        if (this != Instance)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(transform.root.gameObject);
    }
}
