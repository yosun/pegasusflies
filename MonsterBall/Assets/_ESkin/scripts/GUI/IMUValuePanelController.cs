﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Controller for IMU value panel displayed in a pausing panel.
/// </summary>
public class IMUValuePanelController : MonoBehaviour {

    // for displaying values.
    private Text text_;

	// Use this for initialization
	void Start ()
    {
        this.text_ = GetComponentInChildren<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        this.text_.text = SensorManager.Instance.Sensor.GyroRotation.ToString() + " " + SensorManager.Instance.Sensor.GyroVelocity.ToString();
	}
}
