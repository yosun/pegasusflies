﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using ESkin;

/// <summary>
/// 加速度センサーメーター制御クラス
/// </summary>
public class IMUSensorMeterController : MeterController
{
    // 参照しているセンサーのチャンネル
    private IMUSensorChannel _Channel = IMUSensorChannel.ACCEL_X;

    /// <summary>
    /// 参照しているセンサーのチャンネル
    /// </summary>
    public IMUSensorChannel Channel
    {
        set
        {
            _Channel = value;
            SetChannelText(_Channel.ToString());
        }
    }

    protected override int GetSensorCurrentValue()
    {
        return SensorManager.Instance.Sensor.GetFilteredIMUValue(_Channel);
    }

    protected override SensorRange GetSensorMinMaxRange()
    {
        return SensorManager.Instance.Sensor.GetMinMaxRange(_Channel);
    }

    protected override SensorRange GetSensorValueRange()
    {
        return SensorManager.Instance.Sensor.GetMinMaxRange(_Channel);
    }
}
