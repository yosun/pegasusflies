﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using ESkin;

/// <summary>
/// モーションセンサーメーター制御クラス
/// </summary>
public class SensorMeterController : MeterController
{
    // 参照しているセンサーのチャンネル
    private StrainSensorChannel _Channel = StrainSensorChannel.ELBOW_R;

    /// <summary>
    /// 参照しているセンサーのチャンネル
    /// </summary>
    public StrainSensorChannel Channel
    {
        set
        {
            _Channel = value;
            SetChannelText(_Channel.ToString());
        }
    }

    protected override int GetSensorCurrentValue()
    {
        return SensorManager.Instance.Sensor.GetFilteredStrainValue(_Channel);
    }

    protected override SensorRange GetSensorMinMaxRange()
    {
        return SensorManager.Instance.Sensor.GetMinMaxRange(_Channel);
    }

    protected override SensorRange GetSensorValueRange()
    {
        return SensorManager.Instance.Sensor.GetStrainSensorRange(_Channel);
    }
}
