﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ESkin.Unity;
using ESkin;

/// <summary>
/// ポーズ画面制御クラス
/// </summary>
public class PauseCanvasController : DontDestroySingletonMonoBehaviour<PauseCanvasController>
{
    // キャンバス
    private Canvas _Canvas;

    // ポーズキー
    public KeyCode PauseKey;

    // 初期化処理
    new void Awake()
    {
        base.Awake();

        _Canvas = transform.Find("Canvas").GetComponent<Canvas>();
        Transform buttonPanel = _Canvas.transform.Find("PausePanel/ButtonPanel");
        
        // 肘キャリブレーションボタン
        Button armCaliButton = buttonPanel.Find("ArmCalibrationButton").GetComponent<Button>();
        armCaliButton.onClick.AddListener(OnClickArmCalibrationButton);

        // 肩キャリブレーションボタン
        Button shoulderCaliButton = buttonPanel.Find("ShoulderCalibrationButton").GetComponent<Button>();
        shoulderCaliButton.onClick.AddListener(OnClickShoulderCalibrationButton);

        // キャリブレーション停止ボタン
        Button stopCalibrationButton = buttonPanel.Find("StopCalibrationButton").GetComponent<Button>();
        stopCalibrationButton.onClick.AddListener(OnClickStopCalibrationButton);

        // 閉じるボタン
        Button closeButton = buttonPanel.Find("CloseButton").GetComponent<Button>();
        closeButton.onClick.AddListener(OnClickCloseButton);
    }
    
	// 描画処理
	void Update ()
    {
        // キー押下に合わせてポーズ状態を反転させる
        if(Input.GetKeyDown(PauseKey))
        {
            SetPause(!Pausing);
        }
	}

    /// <summary>
    /// ポーズ状態を設定する
    /// </summary>
    /// <param name="pause">ポーズ状態。trueであればポーズ</param>
    public void SetPause(bool pause)
    {
        if(pause)
        {
            //Time.timeScale = 0.0f;
            _Canvas.gameObject.SetActive(true);
        }
        else
        {
            //Time.timeScale = 1.0f;
            _Canvas.gameObject.SetActive(false);

            // キャリブレーション停止
            StopCalibration();
        }
    }

    // キャリブレーション停止
    private void StopCalibration()
    {
        UnityESkinSensor sensor = SensorManager.Instance.Sensor;
        sensor.StopCalibration(StrainSensorChannel.SCAPULA_R);
        sensor.StopCalibration(StrainSensorChannel.ELBOW_R);
        sensor.StopCalibration(StrainSensorChannel.AXILLA_R);
        sensor.StopCalibration(StrainSensorChannel.SHOULDER_R);
        sensor.StopCalibration(StrainSensorChannel.SCAPULA_L);
        sensor.StopCalibration(StrainSensorChannel.ELBOW_L);
        sensor.StopCalibration(StrainSensorChannel.AXILLA_L);
        sensor.StopCalibration(StrainSensorChannel.SHOULDER_L);
        sensor.UpdateGyroOrigin ();
    }

    /// <summary>
    /// ポーズ中か返す
    /// </summary>
    public bool Pausing
    {
        get { return _Canvas.gameObject.activeSelf; }
    }

    // 肘キャリブレーションボタン
    private void OnClickArmCalibrationButton()
    {
        UnityESkinSensor sensor = SensorManager.Instance.Sensor;
        StopCalibration();
		sensor.StartCalibrationForBoth(StrainSensorChannel.SCAPULA_R);
        sensor.StartCalibrationForBoth(StrainSensorChannel.ELBOW_R);
        sensor.StartCalibrationForBoth(StrainSensorChannel.SCAPULA_L);
        sensor.StartCalibrationForBoth(StrainSensorChannel.ELBOW_L);
    }

    // 肩キャリブレーションボタン
    private void OnClickShoulderCalibrationButton()
    {
        UnityESkinSensor sensor = SensorManager.Instance.Sensor;
        StopCalibration();
        sensor.StartCalibrationForBoth(StrainSensorChannel.AXILLA_R);
        sensor.StartCalibrationForBoth(StrainSensorChannel.SHOULDER_R);
        sensor.StartCalibrationForBoth(StrainSensorChannel.AXILLA_L);
        sensor.StartCalibrationForBoth(StrainSensorChannel.SHOULDER_L);
    }

    // キャリブレーション停止ボタン
    private void OnClickStopCalibrationButton()
    {
        StopCalibration();
    }

    // 閉じるボタン押下時の処理
    private void OnClickCloseButton()
    {
        SetPause(false);
    }
}
