﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ESkin.Unity;

/// <summary>
/// センサー設定GUIコントローラークラス
/// </summary>
public class SensorConfigPanelController : MonoBehaviour {

    // センサー名のドロップダウンリスト
    private Dropdown _SensorNameDropdown;

    // 通信速度の入力値
    private InputField _BaudrateInputField;

    // 開始ボタン
    private Button _SensorStartButton;

	// flow control toggle
	private Toggle _FlowControlToggle;
    
    // 起動時の初期化
    void Awake()
    {
        _SensorNameDropdown = transform.Find("SensorNameDropdown").GetComponentInChildren<Dropdown>();
        _BaudrateInputField = transform.Find("BaudrateInputField").GetComponentInChildren<InputField>();
        _SensorStartButton = transform.Find("SensorStartButton").GetComponentInChildren<Button>();
		_FlowControlToggle = transform.Find("FlowControlToggle").GetComponentInChildren<Toggle>();
    }

    // シーン開始時の初期化
    void Start ()
    {
        InitializeSensorNameDropdown();
        InitializeBaudrateInputField();
		InitializeFlowControlToggle();
        InitializeStartButton();
    }
	
    // センサー名のドロップダウンリストを初期化する。
    private void InitializeSensorNameDropdown()
    {
        // ドロップダウンの取得とクリア
        Dropdown dropDown = _SensorNameDropdown;
        dropDown.options.Clear();

        // センサー名取得
        string[] sensorNames = SensorManager.Instance.Sensor.GetSensorNames();
        string currentSensor = SensorManager.Instance.Sensor.SensorName;
        
        // センサー名項目生成
        foreach (string sensorName in sensorNames)
        {
            dropDown.options.Add(new Dropdown.OptionData(sensorName));

            // 現在のセンサーの項目を選択する
            if(sensorName.Equals(currentSensor))
            {
                dropDown.value = dropDown.options.Count - 1;
            }
        }

        // キャプションを更新する
        dropDown.captionText.text = currentSensor;
    }

    // ボーレート入力フィールドの初期化
    private void InitializeBaudrateInputField()
    {
        _BaudrateInputField.text = SensorManager.Instance.Sensor.Baudrate.ToString();
    }

	// Initializes the flow control toggle.
	private void InitializeFlowControlToggle()
	{
		_FlowControlToggle.isOn = SensorManager.Instance.Sensor.FlowControl;
	}

    // スタートボタンの初期化
    private void InitializeStartButton()
    {
        _SensorStartButton.onClick.AddListener(OnClickStart);
    }
    
    /// <summary>
    /// スタートボタン押下時の処理
    /// </summary>
    public void OnClickStart()
    {
        // センサーに設定値を反映する。
        UnityESkinSensor sensor = SensorManager.Instance.Sensor;
        sensor.SensorName = _SensorNameDropdown.options[_SensorNameDropdown.value].text;
        sensor.Baudrate = int.Parse(_BaudrateInputField.text);
		sensor.FlowControl = _FlowControlToggle.isOn;

        // 設定の保存
        sensor.SaveConfiguration();

        // センサー開始
        sensor.Start();

        Debug.LogFormat("Sensor has been opend. name:{0} baudrate:{1}", sensor.SensorName, sensor.Baudrate);
    }

    // ドロップダウンリストが残り続けていたら破棄する。
    public void RemoveDropdownList()
    {
        Transform list = _SensorNameDropdown.transform.FindChild("Dropdown List");
        if(list != null)
        {
            DestroyImmediate(list.gameObject);
        }
    }
}
