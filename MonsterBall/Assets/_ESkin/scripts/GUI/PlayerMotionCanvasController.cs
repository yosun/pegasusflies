﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ESkin.Unity;

/// <summary>
/// プレーヤー動作GUIキャンバスの管理クラス
/// </summary>
public class PlayerMotionCanvasController : DontDestroySingletonMonoBehaviour<PlayerMotionCanvasController>
{
    // キャンバス
    private Canvas _Canvas;

    // 左肩スライダー
    private Slider _LeftShoulderSlider;

    // 左肘スライダー
    private Slider _LeftArmSlider;

    // 右肩スライダー
    private Slider _RightShoulderSlider;

    // 右肘スライダー
    private Slider _RightArmSlider;

    // ジャンプスライダー
    private Slider _JumpSlider;

    // ポーズキー
    public KeyCode PauseKey;

    // 初期化処理
    new void Awake()
    {
        base.Awake();

        // キャンバス取得
        _Canvas = transform.Find("Canvas").GetComponent<Canvas>();
        Transform panel = _Canvas.transform.Find("PlayerMotionPanel");

        // GUI使用切り替えトグル
		Toggle useGuiToggle = panel.Find("Toggle").GetComponent<Toggle>();
		useGuiToggle.onValueChanged.AddListener(OnUseGuiToggle);

        // 左側パネル
        Transform leftPanel = panel.Find("LeftPanel");
        _LeftShoulderSlider = leftPanel.Find("ShoulderSlider").GetComponent<Slider>();
        _LeftArmSlider = leftPanel.Find("ArmSlider").GetComponent<Slider>();

        // 右側パネル
        Transform rightPanel = panel.Find("RightPanel");
        _RightShoulderSlider = rightPanel.Find("ShoulderSlider").GetComponent<Slider>();
        _RightArmSlider = rightPanel.Find("ArmSlider").GetComponent<Slider>();

        // ジャンプパネル
        Transform jumpPanel = panel.Find("JumpPanel");
        _JumpSlider = jumpPanel.Find("JumpSlider").GetComponent<Slider>();

		// shoulder up down button
		Transform buttonPanel = panel.Find ("ButtonPanel");
		Button shoulderDownButton = buttonPanel.Find ("ShoulderDownButton").GetComponent<Button>();
		shoulderDownButton.onClick.AddListener (OnClickShoulderDownButton);
		Button shoulderUpButton = buttonPanel.Find ("ShoulderUpButton").GetComponent<Button>();
		shoulderUpButton.onClick.AddListener (OnClickShoulderUpButton);
    }

    // 毎フレームの処理
    void Update()
    {
        // 設定値を反映する
        PropertyPlayerMotion motion = PlayerMotionManager.Instance.PropertyMotion;
        motion.LeftShoulderValue = _LeftShoulderSlider.value;
        motion.LeftArmValue = _RightArmSlider.value;
        motion.RightShoulderValue = _RightShoulderSlider.value;
        motion.RightArmValue = _RightArmSlider.value;
		motion.LeftFrontValue = _LeftArmSlider.value;
		motion.RightFrontValue = _LeftArmSlider.value;
        motion.JumpValue = _JumpSlider.value;

        // キー押下に合わせてポーズ状態を反転させる
        if (Input.GetKeyDown(PauseKey))
        {
            SetVisible(!Visible);
        }
    }

    /// <summary>
    /// ポーズ状態を設定する
    /// </summary>
    /// <param name="pause">ポーズ状態。trueであればポーズ</param>
    public void SetVisible(bool visible)
    {
        if (visible)
        {
            _Canvas.gameObject.SetActive(true);
        }
        else
        {
            _Canvas.gameObject.SetActive(false);
        }
    }

    // トグルボタン押下時の処理
    private void OnUseGuiToggle(bool useGui)
    {
        PlayerMotionManager.Instance.UseProperty = useGui;
    }
	
	private void OnClickShoulderDownButton()
	{
		_RightShoulderSlider.value = 0;
		_LeftShoulderSlider.value = 0;
	}

	private void OnClickShoulderUpButton()
	{
		_RightShoulderSlider.value = 1;
		_LeftShoulderSlider.value = 1;
	}

    /// <summary>
    /// 表示状態かどうか
    /// </summary>
    public bool Visible { get { return _Canvas.gameObject.activeSelf; } }
}
