﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ESkin;

/// <summary>
/// センサーメーターパネルコントローラークラス
/// </summary>
public class SensorMetersPanelController : MonoBehaviour {

    // センサーメーター部分
    private Image _SensorMeterPanel;

    /// <summary>
    /// センサー値メーター
    /// </summary>
    public GameObject SensorMeter;

    /// <summary>
    /// 加速度センサー値メーター
    /// </summary>
    public GameObject IMUSensorMeter;

    // 起動時の初期化
    void Awake()
    {
        _SensorMeterPanel = transform.GetComponent<Image>();
    }

    // シーン開始時の初期化
    void Start ()
    {
        InitializeSensorMeterPanel();
    }
	
    // センサーメーターを初期化する
    private void InitializeSensorMeterPanel()
    {
		AddSensorMeter(StrainSensorChannel.SCAPULA_R);
        AddSensorMeter(StrainSensorChannel.ELBOW_R);
        AddSensorMeter(StrainSensorChannel.AXILLA_R);
        AddSensorMeter(StrainSensorChannel.SHOULDER_R);

        AddSensorMeter(StrainSensorChannel.SCAPULA_L);
        AddSensorMeter(StrainSensorChannel.ELBOW_L);
        AddSensorMeter(StrainSensorChannel.AXILLA_L);
        AddSensorMeter(StrainSensorChannel.SHOULDER_L);

        //AddSensorMeter(StrainSensorChannel.WRIST_R);
        //AddSensorMeter(StrainSensorChannel.THORAX_R);
        //AddSensorMeter(StrainSensorChannel.BACK_R);
        //AddSensorMeter(StrainSensorChannel.WRIST_L);
        //AddSensorMeter(StrainSensorChannel.THORAX_L);
        //AddSensorMeter(StrainSensorChannel.BACK_L);

        AddSensorMeter(IMUSensorChannel.ACCEL_X);
        AddSensorMeter(IMUSensorChannel.ACCEL_Y);
        AddSensorMeter(IMUSensorChannel.ACCEL_Z);
        AddSensorMeter(IMUSensorChannel.GYRO_X);
        AddSensorMeter(IMUSensorChannel.GYRO_Y);
        AddSensorMeter(IMUSensorChannel.GYRO_Z);
        AddSensorMeter(IMUSensorChannel.MAG_X);
        AddSensorMeter(IMUSensorChannel.MAG_Y);
        AddSensorMeter(IMUSensorChannel.MAG_Z);
    }

    // センサーメーターを追加する
    private void AddSensorMeter(StrainSensorChannel channel)
    {
        GameObject sensorMeter = Instantiate(SensorMeter);
        sensorMeter.GetComponent<SensorMeterController>().Channel = channel;
        sensorMeter.transform.SetParent(_SensorMeterPanel.transform, false);
    }

    // センサーメーターを追加する
    private void AddSensorMeter(IMUSensorChannel channel)
    {
        GameObject sensorMeter = Instantiate(IMUSensorMeter);
        sensorMeter.GetComponent<IMUSensorMeterController>().Channel = channel;
        sensorMeter.transform.SetParent(_SensorMeterPanel.transform, false);
    }
}
