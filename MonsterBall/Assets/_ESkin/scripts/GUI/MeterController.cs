﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ESkin;

/// <summary>
/// メーター制御クラス
/// </summary>
public abstract class MeterController : MonoBehaviour
{
    // チャンネル表示テキスト
    private Text _ChannelText;

    // センサー範囲のイメージ領域
    private RectTransform _SensorRangeRect;

    // センサー値のイメージ領域
    private RectTransform _SensorValueRect;

    // センサー範囲の最小値テキスト
    private Text _RangeMinText;

    // センサー値テキスト
    private Text _CurrentValueText;

    // センサー範囲の最大値テキスト
    private Text _RangeMaxText;

    /// <summary>
    /// 初期化
    /// </summary>
    void Awake()
    {
        _ChannelText = transform.Find("ChannelText").GetComponent<Text>();
        Transform backPanel = transform.Find("SensorMeterBackPanel");
        _SensorRangeRect = backPanel.Find("SensorRangePanel").GetComponent<RectTransform>();
        _SensorValueRect = backPanel.Find("SensorValueImage").GetComponent<RectTransform>();
        _RangeMinText = backPanel.Find("RangeMinText").GetComponent<Text>();
        _CurrentValueText = backPanel.Find("CurrentValueText").GetComponent<Text>();
        _RangeMaxText = backPanel.Find("RangeMaxText").GetComponent<Text>();
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        UpdateSensorRangeImage();
        UpdateSensorValueImage();
    }

    // センサー値イメージの更新
    private void UpdateSensorValueImage()
    {
        int currentValue = GetSensorCurrentValue();
        _CurrentValueText.text = currentValue.ToString();
        SetPosition(_SensorValueRect, currentValue);
    }

    // センサー範囲イメージの更新
    private void UpdateSensorRangeImage()
    {
        SensorRange sensorRange = GetSensorValueRange();
        _RangeMinText.text = sensorRange.MinValue.ToString();
        _RangeMaxText.text = sensorRange.MaxValue.ToString();
        SetPosition(_SensorRangeRect, sensorRange.MinValue);
        SetWidth(_SensorRangeRect, sensorRange.MaxValue - sensorRange.MinValue);
    }

    // センサー値を正規化する
    private float NormalizeSensorValue(int sensorValue)
    {
        SensorRange range = GetSensorMinMaxRange();
        return Mathf.InverseLerp(range.MinValue, range.MaxValue, sensorValue);
    }

    /// <summary>
    /// センサーの現在値を取得する
    /// </summary>
    /// <returns>センサーの現在値</returns>
    protected abstract int GetSensorCurrentValue();

    /// <summary>
    /// センサーの受信値の範囲を取得する
    /// </summary>
    /// <returns>センサーの受信値の範囲</returns>
    protected abstract SensorRange GetSensorValueRange();

    /// <summary>
    /// チャンネル名を設定する
    /// </summary>
    /// <param name="channel">チャンネル名</param>
    protected void SetChannelText(string channel)
    {
        if (_ChannelText != null)
        {
            _ChannelText.text = channel;
        }   
    }

    /// <summary>
    /// センサーの最小・最大範囲を取得する
    /// </summary>
    /// <returns>センサーの最小・最大範囲</returns>
    protected abstract SensorRange GetSensorMinMaxRange();

    // 値をメーター上の座標に補完する
    private float LerpMeterPosition(int sensorValue)
    {
        float t = NormalizeSensorValue(sensorValue);
        float maxPosition = _SensorRangeRect.parent.GetComponent<RectTransform>().rect.width;
        return Mathf.Lerp(0.0f, maxPosition, t);
    }

    // 矩形の位置を設定する
    private void SetPosition(RectTransform rect, int sensorValue)
    {
        float x = LerpMeterPosition(sensorValue);
        rect.localPosition = new Vector3(x, rect.localPosition.y, rect.localPosition.z);
    }

    // 矩形のサイズを設定する
    private void SetWidth(RectTransform rect, int sensorValue)
    {
        float w = LerpMeterPosition(sensorValue);
        Vector2 size = rect.sizeDelta;
        rect.sizeDelta = new Vector2(w, size.y);
    }
}
