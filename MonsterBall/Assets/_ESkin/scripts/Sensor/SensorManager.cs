﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ESkin.Unity;

/// <summary>
/// センサー管理クラス
/// </summary>
public class SensorManager : DontDestroySingletonMonoBehaviour<SensorManager>
{

    // 現在のセンサー
    private UnityESkinSensor _Sensor;

    /// <summary>
    /// 初期化処理
    /// </summary>
    new void Awake()
    {
        base.Awake();

        _Sensor = new UnityESkinSensor();
    }

    /// <summary>
    /// 終了時の処理
    /// </summary>
    void OnDestroy()
    {
        _Sensor.OnDestroy();
    }

    /// <summary>
    /// get sensor.
    /// </summary>
    public UnityESkinSensor Sensor
    {
        get { return _Sensor; }
    }

    /// <summary>
    /// update per fixed time.
    /// </summary>
    void FixedUpdate()
    {
        _Sensor.FixedUpdate();
    }
}