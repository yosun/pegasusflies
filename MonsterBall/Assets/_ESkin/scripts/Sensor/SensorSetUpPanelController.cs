﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ESkin.Unity;
using ESkin;

/// <summary>
/// センサー設定GUIコントローラークラス
/// </summary>
public class SensorSetUpPanelController : MonoBehaviour {

    // センサー名のドロップダウンリスト
    private Dropdown _SensorNameDropdown;

    // 通信速度の入力値
    private InputField _BaudrateInputField;

    // 開始ボタン
    private Button _SensorStartButton;

    // センサーメーター部分
    private Image _SensorMeterPanel;

    /// <summary>
    /// センサー値メーター
    /// </summary>
    public GameObject SensorMeter;

    /// <summary>
    /// 加速度センサー値メーター
    /// </summary>
    public GameObject IMUSensorMeter;

    // 起動時の初期化
    void Awake()
    {
        _SensorNameDropdown = transform.Find("SensorNameDropdown").GetComponentInChildren<Dropdown>();
        _BaudrateInputField = transform.Find("BaudrateInputField").GetComponentInChildren<InputField>();
        _SensorStartButton = transform.Find("SensorStartButton").GetComponentInChildren<Button>();
        _SensorMeterPanel = transform.Find("SensorMeterPanel").GetComponent<Image>();
    }

    // シーン開始時の初期化
    void Start ()
    {
        InitializeSensorNameDropdown();
        InitializeBaudrateInputField();
        InitializeStartButton();
        InitializeSensorMeterPanel();
    }
	
    // センサー名のドロップダウンリストを初期化する。
    private void InitializeSensorNameDropdown()
    {
        // ドロップダウンの取得とクリア
        Dropdown dropDown = _SensorNameDropdown;
        dropDown.options.Clear();

        // センサー名取得
        string[] sensorNames = SensorManager.Instance.Sensor.GetSensorNames();
        string currentSensor = SensorManager.Instance.Sensor.SensorName;
        
        // センサー名項目生成
        foreach (string sensorName in sensorNames)
        {
            dropDown.options.Add(new Dropdown.OptionData(sensorName));

            // 現在のセンサーの項目を選択する
            if(sensorName.Equals(currentSensor))
            {
                dropDown.value = dropDown.options.Count - 1;
            }
        }

        // キャプションを更新する
        dropDown.captionText.text = currentSensor;
    }

    // ボーレート入力フィールドの初期化
    private void InitializeBaudrateInputField()
    {
        _BaudrateInputField.text = SensorManager.Instance.Sensor.Baudrate.ToString();
    }

    // スタートボタンの初期化
    private void InitializeStartButton()
    {
        _SensorStartButton.onClick.AddListener(OnClickStart);
    }
    
    /// <summary>
    /// スタートボタン押下時の処理
    /// </summary>
    public void OnClickStart()
    {
        // センサーに設定値を反映する。
        UnityESkinSensor sensor = SensorManager.Instance.Sensor;
        sensor.SensorName = _SensorNameDropdown.options[_SensorNameDropdown.value].text;
        sensor.Baudrate = int.Parse(_BaudrateInputField.text);

        // 設定の保存
        sensor.SaveConfiguration();

        // センサー開始
        sensor.Start();

        Debug.LogFormat("Sensor has been opend. name:{0} baudrate:{1}", sensor.SensorName, sensor.Baudrate);
    }

    // センサーメーターを初期化する
    private void InitializeSensorMeterPanel()
    {
        AddSensorMeter(StrainSensorChannel.SCAPULA_R);
        AddSensorMeter(StrainSensorChannel.ELBOW_R);
        AddSensorMeter(StrainSensorChannel.AXILLA_R);
        AddSensorMeter(StrainSensorChannel.SHOULDER_R);

        AddSensorMeter(IMUSensorChannel.ACCEL_X);
        AddSensorMeter(IMUSensorChannel.ACCEL_Y);
        AddSensorMeter(IMUSensorChannel.ACCEL_Z);
        AddSensorMeter(IMUSensorChannel.GYRO_X);
        AddSensorMeter(IMUSensorChannel.GYRO_Y);
        AddSensorMeter(IMUSensorChannel.GYRO_Z);
        AddSensorMeter(IMUSensorChannel.MAG_X);
        AddSensorMeter(IMUSensorChannel.MAG_Y);
        AddSensorMeter(IMUSensorChannel.MAG_Z);
    }

    // センサーメーターを追加する
    private void AddSensorMeter(StrainSensorChannel channel)
    {
        GameObject sensorMeter = Instantiate(SensorMeter);
        sensorMeter.GetComponent<SensorMeterController>().Channel = channel;
        sensorMeter.transform.SetParent(_SensorMeterPanel.transform, false);
    }

    // センサーメーターを追加する
    private void AddSensorMeter(IMUSensorChannel channel)
    {
        GameObject sensorMeter = Instantiate(IMUSensorMeter);
        sensorMeter.GetComponent<IMUSensorMeterController>().Channel = channel;
        sensorMeter.transform.SetParent(_SensorMeterPanel.transform, false);
    }
}
