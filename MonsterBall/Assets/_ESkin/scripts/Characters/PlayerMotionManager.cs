﻿using UnityEngine;
using System.Collections;
using ESkin.Unity;

/// <summary>
/// プレーヤーの動作のマネージャークラス
/// </summary>
public class PlayerMotionManager : DontDestroySingletonMonoBehaviour<PlayerMotionManager>
{
    // 現在のプレーヤー動作
    private PlayerMotion _CurrentPlayerMotion;

    // センサー値によるプレーヤー動作インスタンス
    private SensorPlayerMotion _SensorPlayerMotion;

    // 設定値によるプレーヤー動作インスタンス
    private PropertyPlayerMotion _PropertyPlayerMotion;

    /// <summary>
    /// 初期化処理
    /// </summary>
    new void Awake()
    {
        base.Awake();

        _SensorPlayerMotion = new SensorPlayerMotion(SensorManager.Instance.Sensor);
        _PropertyPlayerMotion = new PropertyPlayerMotion();

        _CurrentPlayerMotion = _SensorPlayerMotion;
    }

    /// <summary>
    /// プレーヤー動作
    /// </summary>
    public PlayerMotion Motion { get { return _CurrentPlayerMotion; } }

    /// <summary>
    /// 設定値によるプレーヤー動作
    /// </summary>
    public PropertyPlayerMotion PropertyMotion { get { return _PropertyPlayerMotion; } }

    /// <summary>
    /// センサーによるプレーヤー動作
    /// </summary>
    public SensorPlayerMotion SensorMotion { get { return _SensorPlayerMotion; } }

    /// <summary>
    /// 設定値を使用しているかどうか
    /// </summary>
    public bool UseProperty
    {
        get
        {
            return _CurrentPlayerMotion is PropertyPlayerMotion;
        }
        set
        {
            _CurrentPlayerMotion = value ? (PlayerMotion)_PropertyPlayerMotion : (PlayerMotion)_SensorPlayerMotion;
        }
    }
}
