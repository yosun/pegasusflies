﻿using UnityEngine;
using System.Collections;
using ESkin.Unity;

/// <summary>
/// Controller for a scientist character.
/// </summary>
public class CharacterScientistController : MonoBehaviour {

    // animator for character model.
    private Animator _Animator;

    // initialize this object.
    void Awake()
    {
        _Animator = transform.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        PlayerMotion motionSource = PlayerMotionManager.Instance.Motion;
        _Animator.SetFloat("LeftShoulder", motionSource.LeftShoulderValue);
        _Animator.SetFloat("RightShoulder", motionSource.RightShoulderValue);
        _Animator.SetFloat("LeftArm", motionSource.LeftArmValue);
        _Animator.SetFloat("RightArm", motionSource.RightArmValue);
        _Animator.SetFloat("LeftFront", motionSource.LeftFrontValue);
        _Animator.SetFloat("RightFront", motionSource.RightFrontValue);

        // set up direction by accel (gravity) vector.
        Vector3 up = (-motionSource.Accel).normalized;

        // a left to right ducking angle culculated by angles between left vector to character up vector
        Vector3 upAcoundZ = up;
        upAcoundZ.z = 0.0f;
        _Animator.SetFloat("BodyDuckLeftToRight", (180.0f - Vector3.Angle(Vector3.left, upAcoundZ)) / 180.0f);

        // a back to front ducking angle culculated by angles between back vector to character up vector
        Vector3 upAroundX = up;
        upAroundX.x = 0.0f;
        _Animator.SetFloat("BodyDuckBackToFront", (180.0f - Vector3.Angle(Vector3.back, upAroundX)) / 180.0f);
        
		// apply rotation by Y axis from gyro value.
		transform.rotation = Quaternion.Euler(0.0f, motionSource.GyroRotation.y, 0.0f);
    }
}
