﻿using UnityEngine;
using System.Collections;

public class ShowRendererUponCollide : MonoBehaviour {

    Renderer me;

    void Awake()
    {
        me = GetComponent<Renderer>();

        me.enabled = false;
    }

	void OnCollisionEnter(Collision col)
    {
        //print("COL "+col.name);
        if(col.transform.tag == "Pegasus")
         me.enabled = true;
    }

    void OnCollisionExit(Collision col)
    {
        if (col.transform.tag == "Pegasus")
            me.enabled = false;
    }

}
