﻿using UnityEngine;
using System.Collections;

public class Directions : MonoBehaviour {

    public GameObject goDirections;
    public GameObject goCam;
    public void KillDirections()
    {
        goDirections.SetActive(false);
        FlyWings.paused = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            goDirections.SetActive(true);
            FlyWings.paused = true;
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            goCam.SetActive(true);
        }
    }


    public void KillCamera()
    {
        goCam.SetActive(false);
    }
}
