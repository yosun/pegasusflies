﻿using UnityEngine;
using System.Collections;
using ESkin.Unity;
public class FlyWings : MonoBehaviour {

    public Transform tLeftWingController;
    public Transform tRightWingController;

	public Transform tNeck;

	public Transform tUnicorn;

    public Transform tCamera;

	float lastshoulder=0f;

	float maxHeight = 1.37f;
	float minHeight = -1.618f;

    Vector3 wingLeft;
    Vector3 wingRIght;

    float timePrevFlap = 0f;

    bool lastLeftUp = false; bool lastRightUp = false;

    float neckLast;

    public static bool paused = false;
	public void LeftWingUp(float d)
	{
        Vector3 rot = tLeftWingController.localRotation.eulerAngles;
        tLeftWingController.localRotation = Quaternion.Euler(Mathf.Clamp(rot.x - d, 45f, 67f) , 180f,90f);
    }
	
	public void LeftWingDown(float d)
	{
        Vector3 rot = tRightWingController.localRotation.eulerAngles;
        tRightWingController.localRotation = Quaternion.Euler(Mathf.Clamp(rot.x + d, 45f, 67f), 180f,90f);// wingLeft.y, wingLeft.z);
    }
	
	public void RightWingUp(float d)
    {
        Vector3 rot = tRightWingController.localRotation.eulerAngles;
        tRightWingController.localRotation = Quaternion.Euler(Mathf.Clamp(rot.x - d, 45f, 67f), 360f,270f);// wingRIght.y, wingRIght.z);
         

    }
	
	public void RightWingDown(float d)
	{
        Vector3 rot = tRightWingController.localRotation.eulerAngles;


        tRightWingController.localRotation = Quaternion.Euler(Mathf.Clamp(rot.x + d, 45f, 67f), 360f,270f);
    }

    public void LeftWingAbs(float d)
    {
        Vector3 rot = tLeftWingController.localRotation.eulerAngles;
     //   print(lastRightUp);
        if (d > 0.5f) { d = 0.1f; if (!lastLeftUp) AdvanceFwd(); lastLeftUp = true; }
        else if (d < 0.5f) { d = -0.1f; if (lastLeftUp) AdvanceFwd(); lastLeftUp = false; }
        else d = 0f;
        tLeftWingController.localRotation = Quaternion.Euler(Mathf.Clamp(rot.x + d, 40f, 77f), 360f,270f);
    }

    public void RightWingAbs(float d)
    {
        Vector3 rot = tRightWingController.localRotation.eulerAngles;
//print(lastRightUp);
        if (d > 0.5f) { d = 0.1f; if (!lastRightUp) AdvanceFwd();  lastRightUp = true; }
        else if (d < 0.5f) { d = -0.1f; if (lastRightUp) AdvanceFwd(); lastRightUp = false; }
        else d = 0f;
        tRightWingController.localRotation = Quaternion.Euler(Mathf.Clamp(rot.x + d, 40f, 77f), 180f, 90f);
    }
	 
    public void WingUp()
    {
        timePrevFlap = Time.time;
        RightWingUp(1f);
        LeftWingUp(1f);

    }

    public void WingDown()
    {
        timePrevFlap = Time.time;
        RightWingDown(1f);
        LeftWingDown(1f);

    }

	public void WingUp(float d)
    {
        timePrevFlap = Time.time;
        RightWingUp(d);
		LeftWingUp(d);


         
        if (tUnicorn.position.y < maxHeight)
			tUnicorn.position += new Vector3 (0, 0.01f, 0);
	}
	
	public void WingDown(float d)
    {
        timePrevFlap = Time.time;
        RightWingDown(d);
		LeftWingDown(d);

         
        if (tUnicorn.position.y > minHeight)
			tUnicorn.position -= new Vector3 (0, 0.01f, 0);
	}
	
	// Use this for initialization
	void Start () {
        wingLeft = tLeftWingController.eulerAngles;
        wingRIght = tRightWingController.eulerAngles;

    }

    void AdvanceFwd()
    {
        // return;
        Vector3 ini = tUnicorn.position;
        tUnicorn.position = Vector3.Lerp( ini,ini + tCamera.forward * 0.5f, 1f);
        /*  float dt = Time.time - timePrevFlap;
        //  print(dt);
          if (dt < 0.01f)
          {

          }*/
    }
	
	// Update is called once per frame
	void Update ()
	{
        if (paused) return;

		PlayerMotion motionSource = PlayerMotionManager.Instance.Motion;

		float leftshoulder = motionSource.LeftShoulderValue;
		float rightshoulder = motionSource.RightShoulderValue;

          float rightarm = motionSource.RightArmValue  ; print(rightarm);
        float leftarm = motionSource.LeftArmValue;

         float neck = motionSource.Accel.normalized.x;

		//print (rightshoulder +" "+ leftshoulder);
		float delta = rightshoulder - lastshoulder; float dm = Mathf.Abs (delta);

        /*if (delta < -0.12f)
			WingDown (dm * 10f);
		else if(delta > 0.12f)
			WingUp (dm * 10f); 

		lastshoulder = rightshoulder;*/

        LeftWingAbs(leftshoulder);
        RightWingAbs(rightshoulder);

        if (leftarm > 0.2f)
            TurnRight();
        if (rightarm > 0.2f)
            TurnLeft();

        /*
        if (rightshoulder > 0.7f) {
			float diff = Mathf.Abs (rightshoulder - 0.7f) / 0.2f;
			//tRightWingController.rotation *= Quaternion.Euler(90, 90 + (, 0);
			if(diff > 0.05f)
				WingUp (diff);
		} else if (rightshoulder < 0.5f) {
			float diff = Mathf.Abs (rightshoulder - 0.5f) / 0.5f;
			if(diff > 0.05f)
				WingDown (diff);
		}
		lastshoulder = rightshoulder;


        */
        /*if (rightshoulder > 0.6f) {
			tRightWingController.rotation = Quaternion.Euler(90f, ( 1 - Mathf.Abs ((1f - rightshoulder)/0.4f) )* 90f , 0);
		} else if (rightshoulder < 0.4f) {

		}
		lastshoulder = rightshoulder; */

        if (Input.GetKey(KeyCode.UpArrow))
        {
            WingUp();
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            WingDown();
        }

        if (Input.GetKey(KeyCode.A))
        {
            RightWingUp(1f);
        }
        else if (Input.GetKey(KeyCode.Z))
        {
            RightWingDown(1f);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
            TurnLeft();
        else if (Input.GetKey(KeyCode.RightArrow))
            TurnRight();

      //  AdvanceFwd();
    }


    void TurnLeft()
    {
        //tUnicorn.transform.position -= tUnicorn.transform.right * 0.5f;
        tUnicorn.transform.rotation *= Quaternion.Euler(0, 0.1f, 0);
    }

    void TurnRight()
    {
        //   tUnicorn.transform.position += tUnicorn.transform.right * 0.5f;
        tUnicorn.transform.rotation *= Quaternion.Euler(0, -0.1f, 0);
    }

    void MoveUp()
    {


    }

    void MoveDown()
    {

    }

}
